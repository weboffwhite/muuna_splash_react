var React = require('react');

var Section = React.createClass({

    render: function() {

        var sectionTitle     = (this.props.img) ? <div><img src={this.props.img} className="section-img-title" /> </div> : <h2 className ="section-title">{this.props.title}</h2>;
        var sectionCallToAct = (this.props.callToAction) ? <a href={(this.props.callToActionLink) ? this.props.callToActionLink : "#"} className="btn muuna-plain"><span>{this.props.callToAction}</span></a> : "";

        return (
            <div className={"clearfix muuna-section " + this.props.name}>
                <div id={this.props.name} className="container">
                    <div className="row">
                        <div className="col-xs-12">
                            {sectionTitle}
                            <div className="section-content" dangerouslySetInnerHTML={{__html: this.props.content}} />
                            {sectionCallToAct}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Section;
