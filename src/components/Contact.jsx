var React = require('react');

var Contact = React.createClass({

    render: function() {

        return (
            <div className={"clearfix muuna-section " + this.props.name}>
                <div id="contact" className="container">

                    <div className="row">
                        <div className="col-xs-12">

                            <h5 className="contact-pretitle">Contact Us</h5>
                            <h3 className="contact-title">Get in Touch</h3>
                            <p className="contact-description">We would love to hear your questions or comments.</p>

                            <div className="contact-emails-container">

                                <div className="contact-list-trigger-container">
                                    <a className="contact-list-trigger" href="#0">
                                        <span className="contact-list-title">Select a department...</span>
                                        <span className="contact-list-icon">
                                            <i className="fa fa-sort-desc fa-fw"></i>
                                        </span>
                                    </a>
                                    <div className="contact-list-container">
                                        <ul className="contact-list">
                                            <li className="contact-list-item"><a href="mailto:askus@muuna.com" target="_top">Ask Us</a></li>
                                            <li className="contact-list-item"><a href="mailto:retailers@muuna.com" target="_top">Retailers</a></li>
                                            <li className="contact-list-item"><a href="mailto:media@muuna.com" target="_top">Media</a></li>
                                            <li className="contact-list-item"><a href="mailto:misc@muuna.com" target="_top">Other</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Contact;