var React          = require('react');
var Header         = require('./Header.jsx');
var Visual         = require('./Visual.jsx');
var ProductSlider  = require('./ProductSlider.jsx');
var Callouts       = require('./Callouts.jsx');
var About          = require('./About.jsx');
var Contact        = require('./Contact.jsx');
var Footer         = require('./Footer.jsx');
var Terms          = require('./Terms.jsx');
var Privacy        = require('./Privacy.jsx');
var OhMyGoodness   = require('./OhMyGoodness.jsx');
var FindUs         = require('./FindUs.jsx');
var NutritionFacts = require('./modals/NutritionFacts.jsx');


var Home = React.createClass({

    render: function() {

        /* About */
        var aboutName      = "about";
        var aboutTitle     = "images/new/the-new-way-to-cottage.png";
        var aboutContent   = "<p>It’s time to embrace a lost love—the wholesome, delightful taste of cottage cheese perfected with delicious real fruit. Only, forget about the boring cottage cheese brands we grew up with. Let’s rekindle this love affair the right way, without settling for the stuff that’s been lingering in the shadow of yet another yogurt brand. Times have changed, not to mention what we eat, so we’re breathing new life into cottage cheese with our uniquely contemporary recipe that’ll leave you whispering, “are you kidding me with all this creamy deliciousness?”</p><p>Even better, with its protein-packed goodness, Muuna Cottage Cheese blends your health-conscious living choices together with your desire for smoother, creamier, heavenly taste in every spoonful. So how’d we master the art of the modern dairy offering? It started with a simple decision to ditch the ordinary cottage cheese recipes of decades gone by. With our fresh perspective unlocking worlds of culinary creativity, we headed to the kitchen to reimagine the entire notion of “creamy.” We pulled it off, too! Muuna’s culinary artists recreated cottage cheese with a craveable creamier texture—like, melt in your mouth creamy—and divine taste that brings a satisfied smile to your lips.</p>";
        var aboutCopyTitle = "Healthy, Delightfully Delicious Pleasure Breaks with Muuna™ Cottage Cheese"

        /* Contact */
        var contactName          = "contact";
        var contactTitle         = "Get in Touch";
        var contactContent       = "<p>Questions about Muuna Cottage Cheese, its always-tempting taste, high protein, minimal milkfat, real fruit, or overall nutritional qualities? Care to offer premium Muuna Cottage Cheese in your market? Contact a Muuna representative now:</p>";
        var contactLink          = "mailto:jorge@offwhitedesign.com";
        var contactCallToActCopy = "Send us a Message";

        var sliderImg1 = "images/new/slide-1.jpg";
        var sliderImg2 = "images/new/slide-2.jpg";
        var sliderImg3 = "images/new/slide-3.jpg";

        return (
            <div class="muuna-home">

                <Header />

                <main role="main" id="main">

                    <Visual ie9ImgSrc="images/img-320.jpg, images/img-320-x.jpg 2x" sliderImg1={sliderImg1} sliderImg2={sliderImg2} sliderImg3={sliderImg3} />

                    <ProductSlider />

                    <About />

                    <OhMyGoodness />
                    
                    <FindUs />

                    <Contact title={contactTitle} name={contactName} content={contactContent} callToAction={contactCallToActCopy} callToActionLink={contactLink} />

                    <Callouts />

                </main>

                <Footer />

                <NutritionFacts />

                <Terms />

                <Privacy />

            </div>
        );
    }
});

module.exports = Home;
