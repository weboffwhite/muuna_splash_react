var React = require('react');

var FindUs = React.createClass({
	render: function() {
		return (
			<div className="find-us clearfix">
				<div className="find-us-title-section">
					<h5 id="find-us" className="contact-pretitle">Find Muuna</h5>
					<h3 className="contact-title find-us-title">Currently Available at:</h3>

					<div className="find-us-currently-available clearfix">
						<div className="available-location"><img src="images/new/store-19-shoprite.jpg" /></div>
						<div className="available-location"><img src="images/new/store-20-shaws.jpg" /></div>
					</div>
				</div>

				<div className="find-us-coming-soon clearfix">
					<div className="closing-soon-msg-container">
						<p className="coming-soon-msg">Coming Soon to the following stores:</p>
					</div>
					<div className="muuna-stores">
						<div className="container">
							<div className="muuna-store"><img src="images/new/store-1-pioneer.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-2-foodtown.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-3-keyfoods.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-4-roche.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-5-hannaford.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-6-kings.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-7-acme.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-8-met.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-9-compare.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-10-ctown.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-11-bravo.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-12-bazaar.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-13-morton.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-14-dagostino.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-15-kullen.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-16-finefare.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-17-bigy.jpg" /></div>
							<div className="muuna-store"><img src="images/new/store-18-daves.jpg" /></div>
						</div>
					</div>
				</div>
			</div>
		);
	}
});

module.exports = FindUs;