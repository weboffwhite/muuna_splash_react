var React = require('react');

var About = React.createClass({

    render: function() {

        // var sectionTitle     = (this.props.img) ? <div><img src="images/new/creamiest-cottage-on-earth.png" className="section-img-title" /> </div> : <h2 className ="section-title">{this.props.title}</h2>;

        return (
            <div className="clearfix about">
                <div id="about" className="container">
                    <div className="row about-inner clearfix">

                        <div className="about-copy-container">
                            <img src="images/new/the-new-way-to-cottage.png" className="about-img" />
                            <p className="about-copy">Bye Bye Boring. Hello Muuna. Hello to the irresistibly craveable taste of Muuna™ cottage cheese—perfected with our delectable, have-to-have-it real pieces of fruit on the bottom. Cottage cheese? We know, hard to believe.  But our proprietary recipe will leave you whispering, “Are you kidding me with all this <span className="italic">melt-in-your-mouth</span> creamy deliciousness?” In fact, we think you’ll fall in love with our cottage cheese that’s been totally reimagined, down to the beautifully bright, perfectly portioned new cup.</p>
                            <p className="about-copy">Unlike many guilty pleasures, you’ll be proud to bring your new love home. That’s because Muuna cottage cheese is not only rich and creamy, it’s also rich in protein.  And Muuna contains zero artificial flavors, colors, or sweeteners.  All that goodness and yet the heart wants more…the heart wants passion. Just one spoonful of our ridiculous creaminess married with the orchard fresh taste of our fruit and you’ll wonder where have we been all your life. Muuna, the new way to cottage.</p>
                        </div>

                        <div className="about-img-container">
                            <img src="images/new/about-us-jordan-image.png" alt="" className="about-img girl" />
                        </div>

                    </div>
                </div>
            </div>
        );
    }
});

module.exports = About;