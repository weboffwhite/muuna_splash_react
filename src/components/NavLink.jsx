var React = require('react');

var NavLink = React.createClass({
    
    render: function() {

        var linkName = (this.props.icon) ? <i className={"fa fa-" + this.props.icon + " fa-fw"} /> : this.props.title;

        return (
            <li>
                <a href={this.props.linkAddress} target="_blank">
                    <span>{linkName}</span>
                </a>
            </li>
        );
    }
});

module.exports = NavLink;
