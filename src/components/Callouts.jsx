var React   = require('react');
var Callout = require('./callouts/Callout.jsx');

var counter = 0;
var callouts = [
    {
        "id": counter++,
        "img": "muuna-footer-highinprotein-item1.png",
        "title": "Excellent Source<br />of Protein",
        "content": "Muuna Cottage Cheese is packed with up to 20 grams of protein per single-serve cup. That’s more protein than even Greek yogurt offers, making Muuna the smart choice for your on-the-go lifestyle."
    },
    {
        "id": counter++,
        "img": "muuna-footer-noartificialflavors-item2.png",
        "title": "Free of Artificial<br />Flavors",
        "content": "Made with high-quality ingredients including delicious, real fruit, Muuna Cottage Cheese has all the mouth-pleasing flavor you could want—with none of the artificial flavoring you don’t."
    },
    {
        "id": counter++,
        "img": "muuna-footer-noartificalsweeteners-item3.png",
        "title": "Free of Artificial<br />Sweeteners",
        "content": "High-quality ingredients, real fruit, and our modern cottage cheese recipe means Muuna is satisfyingly sweet without adding artificial sweeteners, like high-fructose corn syrup, that are, less than healthy."
    },
    {
        "id": counter++,
        "img": "muuna-footer-glutenfree-item4.png",
        "title": "Gluten-Free,<br />Kosher",
        "content": "Haven’t you earned the reward of clean, healthful goodness that tastes terrific and satisfies your natural cravings? Of course you have! So Muuna Cottage Cheese is gluten free and made for Kosher diets."
    }
];

var Callouts = React.createClass({
    render: function() {
        var listOfCallouts = callouts.map(function(callout) {
            return (<Callout key={callout.id} img={callout.img} title={callout.title} content={callout.content} />);
        });

        return (
            <div id="footer-callouts" className="muuna-section clearfix">
                <div className="container">
                    <div className="row">
                        {listOfCallouts}
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = Callouts;
