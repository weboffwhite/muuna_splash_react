var React   = require('react');
var NavLink = require('./NavLink.jsx');

var navLinksCounter = 0;
var navLinks = [
    { "id": navLinksCounter++, "title": "Home",     "target": "#muuna"    },
    { "id": navLinksCounter++, "title": "Products", "target": "#products" },
    { "id": navLinksCounter++, "title": "About",    "target": "#about"    },
    { "id": navLinksCounter++, "title": "Find Us",  "target": "#find-us"  },
    { "id": navLinksCounter++, "title": "Contact",  "target": "#contact"  }
];

navLinksCounter = 0;
var navIcons = [
    { "id": navLinksCounter++, "title": "facebook",     "target": "https://facebook.com/Muuna"                                                      },
    { "id": navLinksCounter++, "title": "instagram",    "target": "https://instagram.com/MuunaCottage"                                              },
    { "id": navLinksCounter++, "title": "twitter",      "target": "https://twitter.com/MuunaCottage"                                                },
    { "id": navLinksCounter++, "title": "pinterest",    "target": "https://pinterest.com/MuunaCottage"                                              },
    { "id": navLinksCounter++, "title": "google-plus",  "target": "https://plus.google.com/u/0/b/114167471729458687712/114167471729458687712/about" },
    { "id": navLinksCounter++, "title": "youtube-play", "target": "https://youtube.com/channel/UClLbi842zuJbbNqxOxgmLSg"                            },
    { "id": navLinksCounter++, "title": "vimeo",        "target": "https://vimeo.com/MuunaCottage"                                                  }
];

var Footer = React.createClass({

    render: function() {

        var listOfNavLinks = navLinks.map(function(link) {
            return (<NavLink key={link.id} title={link.title} linkAddress={link.target} />);
        });

        var listOfNavIcons = navIcons.map(function(icon) {
            return (<NavLink key={icon.id} icon={icon.title} linkAddress={icon.target} />);
        });

        return (
            <footer id="footer" className="muuna-section">

                <div className="container">

                    <img src="images/logo.png" alt="Muuna COTTAGE CHEESE" className="footer-logo" />

                    <div id="muuna-navbar" className="footer-nav clearfix">
                        <ul className="muuna-footer-links" role="tablist">{listOfNavLinks}</ul>
                    </div>


                    <div className="footer-copy-container">

                        <p className="footer-follow-us">Follow us on:</p>

                        <div className="footer-social-nav">
                            <ul className="footer-nav">{listOfNavIcons}</ul>
                        </div>

                        <a href="#" className="btn btn-success back-to-top-btn btn-desktop" id="back-to-top">
                            <span><i className="fa fa-arrow-up fa-fw"></i></span>
                        </a>

                        <div className="secondary-nav">
                            <ul className="footer-nav">
                                <li><a href="#" className="terms-trigger" type="button" data-toggle="modal" data-target="#muunaTerms">Terms &amp; Conditions</a></li>
                                <li><a href="#" className="privacy-trigger" type="button" data-toggle="modal" data-target="#muunaPrivacy">Privacy Policy</a></li>
                            </ul>
                        </div>

                        <p className="muuna-copyright footer-copy">
                            Copyright © 2016 <a href="#">Muuna Inc.</a> All Rights Reserved
                        </p>

                    </div>

                    <div className="footer-btn-container">
                        <a href="#" className="btn btn-default back-to-top-btn btn-mobile" id="back-to-top">
                            <span><i className="fa fa-arrow-up fa-fw"></i></span>
                        </a>
                    </div>

                </div>

            </footer>
        );
    }
});

module.exports = Footer;
