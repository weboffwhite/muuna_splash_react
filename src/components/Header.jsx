var React = require('react');
var NavLink = require('./NavLink.jsx');

var navLinksCounter = 0;
var navLinks = [
    { "id": navLinksCounter++, "title": "Home",     "target": "#muuna"    },
    { "id": navLinksCounter++, "title": "Products", "target": "#products" },
    { "id": navLinksCounter++, "title": "About",    "target": "#about"    },
    { "id": navLinksCounter++, "title": "Find Us",  "target": "#find-us"  },
    { "id": navLinksCounter++, "title": "Contact",  "target": "#contact"  }
];

navLinksCounter = 0;
var navIcons = [
    { "id": navLinksCounter++, "title": "facebook",  "target": "https://facebook.com/Muuna"         },
    { "id": navLinksCounter++, "title": "instagram", "target": "https://instagram.com/MuunaCottage" },
    { "id": navLinksCounter++, "title": "twitter",   "target": "https://twitter.com/MuunaCottage"   },
    { "id": navLinksCounter++, "title": "pinterest", "target": "https://pinterest.com/MuunaCottage" }
];

var Header = React.createClass({
    render: function() {

        var listOfNavLinks = navLinks.map(function(link) {
            return (<NavLink key={link.id} title={link.title} linkAddress={link.target} />);
        });

        var listOfNavIcons = navIcons.map(function(icon) {
            return (<NavLink key={icon.id} icon={icon.title} linkAddress={icon.target} />);
        });

        return (
            <header id="header">

                <div className="container header-container">

                    <div className="inner-container">

                        <div className="logo">
                            <a href="#">
                                <img src="images/logo.png" alt="Muuna COTTAGE CHEESE" width={224} height={53} />
                            </a>
                        </div>

                        <div id="muuna-navbar" className="main-navigation">
                            <ul className="muuna-links nav nav-tabs" role="tablist">{listOfNavLinks}</ul>
                        </div>

                        <div id="muuna-hashtag">
                            <span>#muuna</span>
                        </div>

                        <div id="header-social-icons">
                            <ul className="muuna-links social">{listOfNavIcons}</ul>
                        </div>
                    </div>

                </div>

            </header>
        );
    }
});

module.exports = Header;
