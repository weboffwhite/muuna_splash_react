var React = require('react');

var OhMyGoodness =  React.createClass({
	render: function() {
		return (
			<div className="oh-my-goodness clearfix">
				<div className="container">
					<img src="images/new/oh-my-goodness.png" className="oh-my-img-title" />
					<p className="oh-my-description">Compare Muuna to your favorite cottage cheese or your favorite greek yogurt.</p>
					<div className="oh-my-callouts">
						<div className="oh-my-callout"><img src="images/new/oh-my-strawberry-cup.png" className="callout-img" /></div>
						<div className="oh-my-callout"><img src="images/new/oh-my-blueberry-cup.png" className="callout-img" /></div>
						<div className="oh-my-callout"><img src="images/new/oh-my-lowfatplain-cup.png" className="callout-img" /></div>
					</div>
					<p className="oh-my-closing">*<a href="#products" className="oh-my-closing-link">Click here</a> for specific ingredient and nutrient information.</p>
				</div>
			</div>
		);
	}
});

module.exports = OhMyGoodness;