var React = require('react');

var Callout = React.createClass({
    render: function() {
        return (
            <div className="col-xs-12 col-sm-6 col-md-3 footer-callout">
                <img src={"images/new/" + this.props.img} alt={this.props.title} className="section-img"/>
                <h4 className="section-title" dangerouslySetInnerHTML={{__html: this.props.title}} />
                {/* <p className="section-content">{this.props.content}</p> */}
            </div>
        );
    }
});

module.exports = Callout;
