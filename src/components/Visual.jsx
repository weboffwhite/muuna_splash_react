var React = require('react');

var Visual = React.createClass({
    componentDidMount: function() {
        // $('.carousel').carousel();
    },
    render: function() {
        
        var sliderImg1 = { backgroundImage: "url(" + this.props.sliderImg1 + ")" }
        var sliderImg2 = { backgroundImage: "url(" + this.props.sliderImg2 + ")" }
        var sliderImg3 = { backgroundImage: "url(" + this.props.sliderImg3 + ")" }

        return (
            <div className="visual">

                <ul className="social-icons">
                    <li><a href="https://facebook.com/Muuna" target="_blank"><span><i className="fa fa-facebook fa-fw"></i></span></a></li>
                    <li><a href="https://twitter.com/MuunaCottage" target="_blank"><span><i className="fa fa-twitter fa-fw"></i></span></a></li>
                    <li><a href="https://pinterest.com/MuunaCottage" target="_blank"><span><i className="fa fa-pinterest fa-fw"></i></span></a></li>
                    <li><a href="https://instagram.com/MuunaCottage" target="_blank"><span><i className="fa fa-instagram fa-fw"></i></span></a></li>
                </ul>

                <picture>
                    {/*[if IE 9]><video style="display: none;"><![endif]*/}
                    <source srcSet={this.props.ie9ImgSrc} media="(max-width: 767px)" />
                    {/*[if IE 9]></video><![endif]*/}
                </picture>

                <div id="muuna_hero_slider" className="carousel slide muuna-carousel carousel-fade" data-ride="carousel">

                    <ol className="carousel-indicators">
                        <li data-target="#muuna_hero_slider" data-slide-to="0" className="active"></li>
                        <li data-target="#muuna_hero_slider" data-slide-to="1"></li>
                        <li data-target="#muuna_hero_slider" data-slide-to="2"></li>
                    </ol>

                    <div className="carousel-inner" role="listbox">

                        <div className="item active" style={sliderImg3}>
                            <div className="carousel-caption">
                                <h5 className="carousel-pretitle">The New Way to Cottage<span className="muuna-trademark">&trade;</span></h5>
                                <h2 className="carousel-title">RIDICULOUSLY<br />CREAMY</h2>
                            </div>
                        </div>

                        <div className="item" style={sliderImg1}>
                            <div className="carousel-caption">
                                <h5 className="carousel-pretitle">The New Way to Cottage<span className="muuna-trademark">&trade;</span></h5>
                                <h2 className="carousel-title">15g OF PROTEIN<br />GOODNESS</h2>
                            </div>
                        </div>

                        <div className="item" style={sliderImg2}>
                            <div className="carousel-caption">
                                <h5 className="carousel-pretitle">The New Way to Cottage<span className="muuna-trademark">&trade;</span></h5>
                                <h2 className="carousel-title">WHERE HAVE YOU<br />BEEN ALL MY LIFE?<span className="muuna-trademark">&trade;</span></h2>
                            </div>
                        </div>

                    </div>

                    <a className="left carousel-control" href="#muuna_hero_slider" role="button" data-slide="prev">
                        <span className="fa fa-angle-left" aria-hidden="true"></span>
                        <span className="sr-only">Previous</span>
                    </a>

                    <a className="right carousel-control" href="#muuna_hero_slider" role="button" data-slide="next">
                        <span className="fa fa-angle-right" aria-hidden="true"></span>
                        <span className="sr-only">Next</span>
                    </a>
                </div>

            </div>
        );
    }
});

module.exports = Visual;
