var React = require('react');

var NutritionFacts = React.createClass({

    componentDidMount: function() {

        $('#nutritionFacts').on('show.bs.modal', function(event) {

            var button    = $(event.relatedTarget);
            var recipient = button.data('nutrition');
            var modal     = $(this);
            var $img      = $('#nutrition-facts-img');
            var $label    = $('#nutrition-facts-label');

            switch (recipient) {
                case "@strawberry":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-strawberry.jpg');
                    $label.text('Strawberry').removeClass().addClass('modal-title nutrition-facts-label color-strawberry');
                    break;
                case "@blueberry":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-blueberry.jpg');
                    $label.text('Blueberry').removeClass().addClass('modal-title nutrition-facts-label color-blueberry');
                    break;
                case "@pineapple":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-pineapple.jpg');
                    $label.text('Pineapple').removeClass().addClass('modal-title nutrition-facts-label color-pineeapple');
                    break;
                case "@mango":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-mango.jpg');
                    $label.text('Mango').removeClass().addClass('modal-title nutrition-facts-label color-mango');
                    break;
                case "@peach":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-peach.jpg');
                    $label.text('Peach').removeClass().addClass('modal-title nutrition-facts-label color-peach');
                    break;
                case "@plain":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-plain.jpg');
                    $label.text('2% Lowfat Plain').removeClass().addClass('modal-title nutrition-facts-label color-plain');
                    break;
                case "@plain-lowfat":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-plain-lowfat.jpg');
                    $label.text('2% Lowfat Plain').removeClass().addClass('modal-title nutrition-facts-label color-plain');
                    break;
                case "@plain-classic":
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-plain-classic.jpg');
                    $label.text('4% Classic Plain').removeClass().addClass('modal-title nutrition-facts-label color-plain-classic');
                    break;
                default:
                    $img.attr('src', 'images/new/nutrition-facts/nutrition-facts-plain.jpg');
                    $label.text('Muuna').removeClass().addClass('modal-title nutrition-facts-label color-plain');
            }
        });
    },

    render: function() {
        return (
            <div className="modal fade" id="nutritionFacts" tabIndex={-1} role="dialog" aria-labelledby="exampleModalLabel">
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                            <h4 className="modal-title nutrition-facts-label" id="nutrition-facts-label">Muuna Nutrition Facts</h4>
                        </div>
                        <div className="modal-body">
                            <img src="images/new/nutrition-facts/nutrition-facts-plain.jpg" id="nutrition-facts-img" className="nutrition-facts-img" />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = NutritionFacts;
