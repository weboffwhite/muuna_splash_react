var React = require('react');

var PaginationElement = React.createClass({
    render: function() {
        return (
            <li>
                <a href="#">
                    <img src={"images/new/tabs/tab-img0" + this.props.number + ".png"} alt="images description" width={this.props.width} height={this.props.height} />
                </a>
            </li>
        );
    }
});

module.exports = PaginationElement;
