var React = require('react');

var Plain = React.createClass({
    render: function() {
        return(
            <div className={"muuna-product slide plain"}>
                <div className="container slide-content">

                    <span className="name">Plain</span>

                    <div className="cup-holder" data-k="0.3">
                        <div className="img-holder">
                            <picture>
                                {/*[if IE 9]><video style="display: none;"><![endif]*/}
                                <source srcSet="images/new/cup/muuna-cottage-cheese-products-plain-small.png, images/new/cup/muuna-cottage-cheese-products-plain.png 2x" media="(max-width: 767px)" />
                                {/*[if IE 9]></video><![endif]*/}
                                <img src="images/new/cup/muuna-cottage-cheese-products-plain.png" alt="images description" width={370} height={300} />
                            </picture>
                        </div>
                        <a href="#" className="btn muuna-plain" data-toggle="modal" data-target="#nutritionFacts" data-nutrition="@plain"><span>Nutrition Facts</span></a>
                    </div>

                </div>
            </div>
        );
    }
});

