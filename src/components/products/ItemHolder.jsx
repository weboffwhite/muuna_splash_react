var React = require('react');

var ItemHolder = React.createClass({
    render: function() {
        return (
            <div className={"item-holder " + this.props.itemName} data-k={this.props.dataK}>
                <picture>
                    {/*[if IE 9]><video style="display: none;"><![endif]*/}
                    <source srcSet={"images/" + this.props.productName + "-320.png, images/" + this.props.productName + "-320-x.png 2x"} media="(max-width: 767px)" />
                    {/*[if IE 9]></video><![endif]*/}
                    <img src={"images/" + this.props.productName + ".png"} alt="images description" width={370} height={300} />
                </picture>
            </div>
        );
    }
});

module.exports = ItemHolder;
