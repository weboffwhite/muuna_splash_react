var React = require('react');

var ItemHolder = require('./ItemHolder.jsx');

var Product = React.createClass({

    render: function() {

        if (this.props.productName !== "plain") {

            return (
                <div className={"muuna-product slide clearfix " + this.props.productName}>

                    <ItemHolder itemName={"item"} dataK={"0.5"} productName={this.props.productName} />
                    <ItemHolder itemName={"item01"} dataK={"0.5"} productName={this.props.productName + "01"} />

                    <div className="container slide-content">

                        <ItemHolder itemName={"item02"} dataK={"0.4"} productName={this.props.productName + "02"} />
                        <ItemHolder itemName={"item03"} dataK={"0.3"} productName={this.props.productName + "03"} />
                        <ItemHolder itemName={"item04"} dataK={"0.4"} productName={this.props.productName + "04"} />
                        <ItemHolder itemName={"item05"} dataK={"0.4"} productName={this.props.productName + "05"} />
                        <ItemHolder itemName={"item06"} dataK={"0.3"} productName={this.props.productName + "06"} />

                        <span className="name">{this.props.productName}</span>

                        <div className="cup-holder" data-k="0.3">

                            <div className="img-holder">
                                <picture>
                                    {/*[if IE 9]><video style="display: none;"><![endif]*/}
                                    <source srcSet={"images/new/cup/muuna-cottage-cheese-products-" + this.props.productName + "-small.png, images/new/cup/muuna-cottage-cheese-products-" + this.props.productName + ".png 2x" } media="(max-width: 767px)" />
                                    {/*[if IE 9]></video><![endif]*/}
                                    <img src={"images/new/cup/muuna-cottage-cheese-products-" + this.props.productName + ".png"} alt="images description" width={370} height={300} />
                                </picture>
                            </div>
                            <a href="#" className={"btn muuna-" + this.props.productName} data-toggle="modal" data-target="#nutritionFacts" data-nutrition={"@" + this.props.productName}><span>Nutrition Facts</span></a>
                        </div>

                    </div>

                </div>
            );
        } else {
            return (
                <div className={"muuna-product slide " + this.props.productName}>

                    <div className="slide-content">

                        <div id="tabs-container">
                            <ul className="tabs-menu">
                                <li className="current">
                                    <a href="#plain-2-5_3oz" className="clearfix plain-tab-link">
                                        <div className="plain-tab-content-container img-container">
                                            <div className="bg-img reg"></div>
                                        </div>
                                        <div className="plain-tab-content-container copy-container">
                                            <div>
                                                <span className="product-name">2% Lowfat Plain</span>
                                                <span>5.3oz</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#plain-2-16oz" className="clearfix plain-tab-link">
                                        <div className="plain-tab-content-container img-container">
                                            <div className="bg-img lowfat"></div>
                                        </div>
                                        <div className="plain-tab-content-container copy-container">
                                            <div>
                                                <span className="product-name">2% Lowfat Plain</span>
                                                <span>16oz</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                                <li>
                                    <a href="#plain-4-16oz" className="clearfix plain-tab-link classic">
                                        <div className="plain-tab-content-container img-container">
                                            <div className="bg-img classic"></div>
                                        </div>
                                        <div className="plain-tab-content-container copy-container">
                                            <div>
                                                <span className="product-name">4% Classic Plain</span>
                                                <span>16oz</span>
                                            </div>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                            <div className="tab">
                                <div id="plain-2-5_3oz" className="tab-content container">
                                    <span className="name">Plain</span>

                                    <div className="cup-holder" data-k="0.3">
                                        <div className="img-holder">
                                            <picture>
                                                {/*[if IE 9]><video style="display: none;"><![endif]*/}
                                                <source srcSet="images/new/cup/muuna-cottage-cheese-products-plain-small.png, images/new/cup/muuna-cottage-cheese-products-plain.png 2x" media="(max-width: 767px)" />
                                                {/*[if IE 9]></video><![endif]*/}
                                                <img src="images/new/cup/muuna-cottage-cheese-products-plain.png" alt="images description" />
                                            </picture>
                                        </div>
                                        <a href="#" className="btn muuna-plain" data-toggle="modal" data-target="#nutritionFacts" data-nutrition="@plain"><span>Nutrition Facts</span></a>
                                    </div>
                                </div>
                                <div id="plain-2-16oz" className="tab-content container">
                                    <span className="name">Plain</span>

                                    <div className="cup-holder" data-k="0.3">
                                        <div className="img-holder">
                                            <picture>
                                                {/*[if IE 9]><video style="display: none;"><![endif]*/}
                                                <source srcSet="images/new/cup/muuna-cottage-cheese-products-plain-small.png, images/new/cup/muuna-cottage-cheese-products-plain.png 2x" media="(max-width: 767px)" />
                                                {/*[if IE 9]></video><![endif]*/}
                                                <img src="images/new/cup/muuna-products-plain-2-16.png" alt="images description" />
                                            </picture>
                                        </div>
                                        <a href="#" className="btn muuna-plain" data-toggle="modal" data-target="#nutritionFacts" data-nutrition="@plain-lowfat"><span>Nutrition Facts</span></a>
                                    </div>
                                </div>
                                <div id="plain-4-16oz" className="tab-content container">
                                    <span className="name">Plain</span>

                                    <div className="cup-holder" data-k="0.3">
                                        <div className="img-holder">
                                            <picture>
                                                {/*[if IE 9]><video style="display: none;"><![endif]*/}
                                                <source srcSet="images/new/cup/muuna-cottage-cheese-products-plain-small.png, images/new/cup/muuna-cottage-cheese-products-plain.png 2x" media="(max-width: 767px)" />
                                                {/*[if IE 9]></video><![endif]*/}
                                                <img src="images/new/cup/muuna-products-plain-4-16.png" alt="images description" />
                                            </picture>
                                        </div>
                                        <a href="#" className="btn muuna-plain-classic" data-toggle="modal" data-target="#nutritionFacts" data-nutrition="@plain-classic"><span>Nutrition Facts</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            );
        }
    }
});

module.exports = Product;
