var React = require('react');

/* Product */
var Product = require('./products/Product.jsx');
var productCounter = 0;
var products = [
    { "id": productCounter++, "name": "strawberry" },
    { "id": productCounter++, "name": "pineapple"  },
    { "id": productCounter++, "name": "blueberry"  },
    { "id": productCounter++, "name": "mango"      },
    { "id": productCounter++, "name": "plain"      },
    { "id": productCounter++, "name": "peach"      }
];


/* Pagination */
var PaginationElement = require('./products/PaginationElement.jsx');
var paginationCounter = 1;
var paginationElements = [
    { "id": paginationCounter++, "width": 113, "height": 109 },
    { "id": paginationCounter++, "width": 91,  "height": 91  },
    { "id": paginationCounter++, "width": 92,  "height": 91  },
    { "id": paginationCounter++, "width": 92,  "height": 91  },
    { "id": paginationCounter++, "width": 92,  "height": 91  },
    { "id": paginationCounter++, "width": 92,  "height": 91  }
];


var ProductSlider = React.createClass({

    render: function() {

        var paginationList = paginationElements.map(function(pagElement) {
            return (
                <PaginationElement key={pagElement.id} number={pagElement.id} width={pagElement.width} height={pagElement.height} />
            );
        });

        var productList = products.map(function(product) {
            return (
                <Product key={product.id} productName={product.name} />
            );
        });

        return (
            <div className="product-slideshow clearfix">
                <div id="products">
                    <div className="control-holder">
                        <a className="btn-prev icon-arrow-left" href="#" />
                        <a className="btn-next icon-arrow-right" href="#" />

                        <ul className="pagination">{paginationList}</ul>
                    </div>

                    <div className="slideset">
                        <div className="list">{productList}</div>
                    </div>
                </div>
            </div>
        );
    }
});

module.exports = ProductSlider;
